


  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="/home" class="brand-link">
      <img src="img/naotech.jpg" alt="Admin Logo" width="100%" class="brand-image img-circle elevation-2"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Naotech Inc</span>
    </a>


    <div class="sidebar">

      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="img/user.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><strong>{{ Auth::user()->name }}</strong></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
                <router-link to="home" class="nav-link">
                <i class="fa fa-archive nav-icon" aria-hidden="true"></i>
                  <p>
                   Dashboard
                  </p>
                </a>
            </li>

            <li class="nav-item">
                <router-link to="user" class="nav-link">
                  <i class="fa fa-user nav-icon" aria-hidden="true"></i>
                  <p>
                   User Management
                  </p>
                </a>
            </li>

          <li class="nav-item">
            <router-link to="contact" class="nav-link">
              <i class="fa fa-phone-square nav-icon" aria-hidden="true"></i>
              <p>
               Contact
                <span class="right badge badge-danger">{{ $total->count() }}</span>
              </p>
            </a>
          </li>

          <li class="nav-item">
            <router-link to="printers" class="nav-link">
              <i class="fa fa-print nav-icon" aria-hidden="true"></i>
              <p>
                Printer
              </p>
            </a>
        </li>

        <li class="nav-item">
            <router-link to="qoutation" class="nav-link">
              <i class="fas fa-clipboard-check nav-icon"></i>
              <p>
                Quotation
                <span class="right badge badge-danger">{{ $unread->count() }}</span>
              </p>
            </a>
        </li>

        <li class="nav-item">
            <router-link to="hired" class="nav-link">
                <i class="fab fa-hire-a-helper nav-icon"></i>
              <p>
                Hiring
              </p>
            </a>
        </li>

        </ul>
      </nav>
    </div>

  </aside>
