<nav class="main-header navbar navbar-expand navbar-white navbar-light">




    <ul class="navbar-nav">
     <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
     </li>

     <li class="nav-item d-none d-sm-inline-block pt-1">
        @include('admin.search')
      </li>


    </ul>



    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->email }} <span class="caret"></span>
            </a>

            <div class="text-center dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <img src="img/naotech.jpg" alt="Admin Logo" width="50%" class="mb-2 brand-image img-circle elevation-2"
                style="opacity: .8"  onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <div class="row">



                </div>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
  </nav>
