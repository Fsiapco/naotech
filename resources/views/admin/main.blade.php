
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title>Naotech Admin Panel</title>

    <link rel="stylesheet" href="css/app.css">

</head>
<body class="hold-transition sidebar-mini" >
<div class="wrapper"  id="app">


    @include('admin.navbar')


    @include('admin.sidebar')

    @include('admin.content')

    @include('admin.footer')


</div>

<script src="js/app.js"></script>
</body>
</html>
