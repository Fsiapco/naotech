@extends('index')

@section('content')

    @include('partial.carousel')
    <div class="container-fluid " style="background-color:#EDF0F5;">

        <h1 class="text-center headerText pt-5">Welcome! We are Naotech</h1>
        <p class="text-center px-5 mx-5">A Web Services provider in the Philippines. Focus on giving you a dynamic solution to fit your business needs. Let our team manage things for you.</p>

        <div class="row" style="padding: 1% 5% 0;">
            <div class="col-md-8 py-5">
                <video controls autoplay loop  src="{{asset('img/Uncompressed_Services_MOV.mov')}}" type="video/mp4" width="100%">

                </video>
            </div>

            <div class="col-md-4" style="padding:2% 0;">
                <div class="accordion" id="accordionServices">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h5 class="accordionStyleHeader">web application services</h5>
                            </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionServices">
                            <div class="card-body text-justify">
                                <div class="row">
                                    <div class="col-md-2 px-2 py-4">
                                        <i class="fas fa-trophy" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">BEST LOCAL BUSINESS SOLUTION</h6>
                                        <p class="discription-line">Have your business a web presence, our packages was created specifically for start-up business.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 px-2 py-4">
                                        <i class="fas fa-money-bill-alt" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">COST - EFFECTIVE</h6>
                                        <p class="discription-line">All packages is affordable while maintaining good quality. Designs are simple yet powerful.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 px-4 py-4">
                                        <i class="fas fa-mobile-alt" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">MOBILE-RESPONSIVE READY</h6>
                                        <p class="discription-line">Well make sure your site is reachable and readable from smartphones,desktop and tablets.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h5 class="accordionStyleHeader">web hosting services</h5>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionServices">
                            <div class="card-body text-justify">
                                <div class="row">
                                    <div class="col-md-2 px-2 py-4">
                                        <i class="fas fa-server" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">HIGH AVAILABILITY</h6>
                                        <p class="discription-line">Our server is dedicated to keep your website running and available all the time.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 px-2 py-4">
                                        <i class="fas fa-check" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">24 / 7 / 365 UP TIME GUARANTEED</h6>
                                        <p class="discription-line">Up-time-guaranteed, dedicated for your business web presence.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 px-3 py-4">
                                        <i class="fas fa-cubes" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">GREAT BUNDLES</h6>
                                        <p class="discription-line">Specifically designed for all site and app created by Naotech, bound with web basic support.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h5 class="accordionStyleHeader">Printer services</h5>
                            </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionServices">
                            <div class="card-body text-justify">
                            <div class="row">
                                    <div class="col-md-2 px-2 py-4">
                                        <i class="fas fa-thumbs-up" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">AT YOUR SERVICE</h6>
                                        <p class="discription-line">We will deliver and set-up everything. You'll just have to print your work and it's done.</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 px-2 py-4">
                                        <i class="fas fa-medal" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">BRANDED PRINTERS OFFERS</h6>
                                        <p class="discription-line">Great offer from various HP and Canon printers. More choices, more fun!</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 px-3 py-4">
                                        <i class="fas fa-gift" style="font-size:40px;color:gray"></i>
                                    </div>

                                    <div class="col-md-10 pl-2">
                                        <h6 style="font-weight:600">PRACTICAL SOLUTION FOR SME</h6>
                                        <p class="discription-line">It's not just the printer. You'll be having the printer, the toner, the cartridge and Naotech support.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-dark">

        <div class="row text-white">
            <div class="col text-center py-5">
                <h2 class="text-uppercase">Some of our clients & projects </h2>
            </div>
        </div>
        <div class="row ml-5 mr-5 pb-5">
            <div class="clientCarousel justify-content-center w-100">
                <div><img src="{{asset('img/everlearn.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/gomeco.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/grow.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/inspira.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/manalo.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/oriental.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/romago.jpg')}}" class="w-100 p-2" alt=""></div>
                <div><img src="{{asset('img/rc-general-merchandise.jpg')}}" class="w-100 p-2" alt=""></div>
                <div><img src="{{asset('img/emc.jpg')}}" class="w-100 p-2" alt=""></div>
                <div><img src="{{asset('img/dailys-diet.jpg')}}" class="w-100 p-2" alt=""></div>
                <div><img src="{{asset('img/naotech-cc-marigold-falconer.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/naotech-cc-mgfalconer.png')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/mrlabaluvah.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/reliable.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/SKYWORLD.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client2.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client3.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client4.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client5.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client6.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client7.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/client8.jpg')}}" class="w-100 p-2" alt="test"></div>
            </div>
        </div>
        <div class="row ml-5 mr-5 pb-5">
            <div class="web-clientCarousel justify-content-center w-100">
                <div><img src="{{asset('img/web-client1.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/web-client2.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/web-client3.jpg')}}" class="w-100 p-2" alt="test"></div>
                <div><img src="{{asset('img/web-client4.jpg')}}" class="w-100 p-2" alt="test"></div>
            </div>
        </div>
    </div>


    <!-- hiring -->

    <div class="container">
        <div class="row p-3">
            @forelse ($Hiring as $data)
            <div class="col-md-6 pl-5 border rounded border-gray" style="max-height:350px; overflow-y:scroll;">
                <div class="row">
                    <h3 class="text-capitalize pt-2">{{ $data->name }} </h3>
                </div>

            </div>

            <div class="col-md-6" id="hiringPicture">
                <img src="{{asset('hiring/'.$data->photo)}}" class="w-100 border" alt="...">
            </div>
            @empty
            <div class="col-md-6 pl-5 border rounded border-gray" style="max-height:350px; overflow-y:scroll;">
                <div class="row">
                    <h3 class="text-capitalize pt-2">{{--you're what we're looking for--}}No Hiring Posted </h3>
                </div>

            </div>

            <div class="col-md-6" id="hiringPicture">
                <img src="{{asset('img/teaming-up.jpg')}}" class="w-100" alt="">
            </div>
             @endforelse
        </div>
    </div>


@endsection
