<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="landingNav" style="">
  <a class="navbar-brand" href="{{route('index')}}"><img src="{{asset('img/Naotech-WHITE.png')}}" width="250" alt="Naotech Inc"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" >
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
          Services
        </a>
        <div class="dropdown-menu dropleft bg-dark" aria-labelledby="navbarSubDropdown" id="SubDropdown">

          <a class="nav-link dropdown-toggle" href="#" id="navbarSubDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Software Development</a>
            <div class="dropdown-menu bg-dark" aria-labelledby="navbarSubDropdown" style="border: none !important">
              <a class="dropdown-item" href="#">Web Application<a>
              <a class="dropdown-item" href="#">Web Design</a>
              <a class="dropdown-item" href="#">POS System</a>
              <a class="dropdown-item" href="#">E-commerce</a>
              <a class="dropdown-item" href="#">Inventory System</a>
            </div>
          
          <!-- <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">E-mail Hosting<a> -->
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{route('service-Printer')}}">Printer Rental Services</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{route('about-us')}}">About Us <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('contact-us')}}">Contact Us</a>
      </li>
    </ul>
    
  </div>
</nav>
