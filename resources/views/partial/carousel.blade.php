<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">

<div class="carousel-inner" role="listbox">
  <!-- Slide One - Set the background image for this slide in the line below -->
  <div class="carousel-item active" id="firstslide">
    <div class="carousel-caption d-none d-md-block">    
      <p class="line-1">reach wider market</p>
      <p class="line-2">get your own site</p>
      <span class="line-3">cost-effective web design with great after sales!</span><br>
      <button class="btn btn-primary ">request for qoutation</button>
    </div>
  </div>
  <!-- Slide Two - Set the background image for this slide in the line below -->
  <div class="carousel-item" id="secondslide">
    <div class="carousel-caption d-none d-md-block">
      <p class="line-1">make your local business</p>
      <p class="line-2">available online</p>
      <span class="line-3">expose your business and grab the opportunities</span><br>
      <button class="btn btn-primary ">ask us how</button>
    </div>
  </div>
  <!-- Slide Three - Set the background image for this slide in the line below -->
  <div class="carousel-item" id="thirdslide">
    <div class="carousel-caption d-none d-md-block">
      <p class="line-1">designed for SME's</p>
      <p class="line-2">affprdable packages</p>
      <span class="line-3">getting your business online has never been costly!</span><br>
      <button class="btn btn-primary ">request for qoutation</button>
    </div>
  </div>
</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
</div>