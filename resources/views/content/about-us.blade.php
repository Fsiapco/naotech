@extends('index')

@section('content')
<div class="container-fluid  headerGap">
    <div class="row" id="aboutBG">
        <div class="col py-5">
            <div class="row bg-danger border border-danger rounded-right w-25 mt-5" id="abtheaderDesign"> 
                <h2 class=" text-uppercase font-weight-bold text-white pl-3" style="text-shadow:2px 2px 2px black">The Naotech Inc.</h2>
            </div>

            <div class="row pl-5 pt-2">
                <div class="col-md-6">
                    <h3 id="aboutH3">the technology enthusiast</h3>
                    <p class="text-white">Naotech Inc. is an IT company based in the Philippines which aims to be the first and largest IT service provider in Asia and soon dominate globally.</p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row px-5 pt-4">
        <div class="col-md-6"style="padding: 5% 0;" id="order1">
            <h3 id="aboutHistory">our history</h3>
                <p class="pl-5 text-justify" style="font-size:1.5em;line-height:1.3em;">Naotech Inc. was founded in 2015 by Naohiko Sakamoto and Shyr Takagi, being successful in the field of Architecture, Interior Design and House Restoration in their business Takumi Design Ltd. Co.</p>
                <p class="pl-5 text-justify" style="font-size:1.5em;line-height:1.3em;">Mr. Sakamoto often thought of developing and automating its processes to improve their service more and boost client satisfaction via the use of today's technology and apply it to help any businesses in the world, hence Naotech Inc. was born.</p>
        </div>

        <div class="col-md-6 text-center" id="order2">
            <img src="{{asset('/img/boss_sample.png')}}" class="w-75" alt="">
        </div>
    </div>


<div class="container text-center">
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-12">
                    <img src="{{asset('img/Mission-Vision.png')}}" class="w-50 float-left" alt="">
                </div>
                <div class="col">
                    <p class="text-justify px-3 py-5" style="float:right;font-size:1.3em; ">To provide innovative and up to date IT solutions and web services suited for your business needs.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <img src="{{asset('img/Vision.png')}}" class="w-50 float-right" alt="">
                </div>
                <div class="col">
                    <p class="text-justify py-5 px-3" style="font-size:1.3em;">To be the number one service provider of diverse and innovative IT solutions and services.</p>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row" style="margin:4% 0;padding:5% 0 5% 2%;background-color:rgba(248,250,252,0.9); background-blend-mode:overlay; background-image:url('img/naotech-logo.png');background-size:contain;background-position:center;background-repeat:no-repeat;">
    <div class="col">
        <div class="row">
            <div class="col">
                <h3 style="text-transform:uppercase; font-weight:600">innovation</h3>
                <div class="col-md-10 text-justify text" style="font-size:1.2em;">We strive to be the best at innovation on how business processes should be and always be. We embrace diversity as an essential part on how we do our business to contribute positively in our community.</div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h3 style="text-transform:uppercase; font-weight:600">leadership</h3>
                <div class="col-md-10 text-justify" style="font-size:1.2em;">We take prudent risks. Every team member contributes to our success with a deep commitment ti deliver top-notch results. We focus our goals to achieve leadership results, objectives and strategies.</div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <h3 style="text-transform:uppercase; font-weight:600">integrity</h3>
                <div class="col-md-10 text-justify" style="font-size:1.2em;">We follow through on commitments. We are dedicated and take full accountability to our actions. We always try to do the right thing while remaining fair and ethical even in the most defficult situations.</div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection