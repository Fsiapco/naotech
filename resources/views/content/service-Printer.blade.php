@extends('index')

@section('content')

    <div class="container-fluid headerGap" id="printerBG">
        <div class="row py-5">
            <div class="col">
                <!-- <span id="printerHeader">Printer Rental unit at less;</span><br><span id="printerHeader">Make your print out worth the cost</span>   -->
                <img src="{{asset('img/sloganPrinter.png')}}" class="w-50">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container my-3">

            @forelse ($printerDetails as $printer)
                <div class="card bg-light">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <img src="{{asset('printer/'.$printer->photo)}}" class="w-75 border rounded" alt="...">
                            </div>
                            <div class="col-md-6">
                                <h2 class="printerName">{{ $printer->name }}</h2>
                                <table class="table table-borderless my-0 table-sm">
                                    <tbody class="PrinterTable">
                                        <tr>
                                            <td class="printerTable1">Device Type:</td>
                                            <td>{{ $printer->type }}</td>
                                        </tr>
                                        <tr>
                                            <td class="printerTable1">Ink Type:</td>
                                            <td>{{ $printer->ink }}</td>
                                        </tr>
                                        <tr>
                                            <td class="printerTable1">Print Speed:</td>
                                            <td>{{ $printer->speed }}</td>
                                        </tr>
                                        <tr>
                                            <td class="printerTable1">Print Resolution:</td>
                                            <td>{{  $printer->resolution }}</td>
                                        </tr>
                                        <tr>
                                            <td class="printerTable1">Max Paper Sizes:</td>
                                            <td> {{ $printer->size }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button class="btn btn-primary my-1 float-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Request Quotation</button>
                            </div>

                            <div class="collapse card-body" id="collapseExample">
                                <div class="container bg-primart">
                                    <h4>Request Quotation for {{ $printer->name }}</h4>
                                    <form  id="contactUsForm" >
                                                 {{ csrf_field () }}
                                        <div class="form-group">
                                            <input type="text" class="form-control"  name="CompanyName"  id="CompanyName" placeholder="Company Name" value="{{ old('CompanyName') }}"  required>
                                            @if($errors->any())
                                            <h6 class="error text-bold">{{$errors->first('CompanyName')}}</h4>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="Address"  id="Address"  placeholder="Address" value="{{ old('Address') }}"required/>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="contactPerson"  placeholder="Contact Person"id="contactPerson" placcontactPersoneholder="Contact Person" value="{{ old('contactPerson') }}"  required/>
                                        </div>

                                        <div class="form-group">
                                            <input type="email" class="form-control" name="Email"  id="Email" placeholder="E-mail" value="{{ old('Email') }}" required/>
                                        </div>

                                        <div class="form-group">
                                            <input type="number" class="form-control"  min="3" name="contactNumber"   id="contactNumber"   placeholder="Contact Number" value="{{ old('contactNumber') }}"  required/>
                                        </div>

                                        <div class="form-group">
                                            <input type="hidden" class="form-control" value="{{ $printer->name }}" name="Unit" id="Unit" placeholder="Desired Unit">
                                        </div>


                                        <button type="button" id="btnFormSubmit"  class="btn btn-primary w-100 float-right">Send</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @empty
                <div class="text-center">
                    <h1>No Printer Available</h1>
                </div>
            @endforelse


        </div>
    </div>
@endsection

