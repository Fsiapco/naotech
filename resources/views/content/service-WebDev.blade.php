@extends('index')

@section('content')
    <div class="container-fluid headerText2 text-justify headerGap px-5">
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, minima alias eaque molestias corporis laudantium repellendus incidunt sunt rerum commodi dolor adipisci cum laborum nesciunt aut. Eaque aliquam nostrum in!       
        </p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col pt-3">
                <h2 class="text-center">Web Development Packges</h2>
            </div>
        </div>
        <div class="row py-5">
            <div class="col-md-4">
                <div class="card text-center mx-3" style="width: 20rem;">
                    <div class="card-header bg-dark text-white">
                        Standard
                    </div>
                    <div class="card-header bg-dark text-white p-5">
                        <h1>$799</h1>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Design Mockup (PSD)</li>
                        <li class="list-group-item">1-3 Design Revision</li>
                        <li class="list-group-item">1-7 Licensed Photo</li>
                        <li class="list-group-item">1-10 Pages</li>
                        <li class="list-group-item">Secure Sockets Layer (SSL)</li>
                        <li class="list-group-item">Standard Security</li>
                        <li class="list-group-item">30 days Post-Installation Support</li>
                    </ul>
                    <button class="btn btn-danger">Get Started</button>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center mx-3" style="width: 20rem;">
                    <div class="card-header bg-dark text-white">
                        Ecommerce
                    </div>
                    <div class="card-header bg-dark text-white p-5">
                        <h1>$1499</h1>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Design Mockup (PSD)</li>
                        <li class="list-group-item">4-10 Design Revisions</li>
                        <li class="list-group-item">8-20 Licensed Photo</li>
                        <li class="list-group-item">with Content Management System and Payment Integration</li>
                        <li class="list-group-item">11-50 Pages</li>
                        <li class="list-group-item">Secure Sockets Layer (SSL)</li>
                        <li class="list-group-item">Social Media Interrelation</li>
                        <li class="list-group-item">Advance Security</li>
                        <li class="list-group-item">Newsletter Integration</li>
                        <li class="list-group-item">60 days Post-Installation Support</li>
                    </ul>
                    <button class="btn btn-danger">Get Started</button>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center mx-3" style="width: 20rem;">
                    <div class="card-header bg-dark text-white">
                        Enterprise
                    </div>
                    <div class="card-header bg-dark text-white p-5">
                        <h1>$1999</h1>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Design Mockup (PSD)</li>
                        <li class="list-group-item">Unlimited Revision</li>
                        <li class="list-group-item">21-50 Licensed Photo</li>
                        <li class="list-group-item">with Content Management System</li>
                        <li class="list-group-item">Payment Integration (Optional)</li>
                        <li class="list-group-item">Unlimited Pages</li>
                        <li class="list-group-item">Secure Sockets Layer (SSL)</li>
                        <li class="list-group-item">Social Media Interrelation</li>
                        <li class="list-group-item">Premium Security</li>
                        <li class="list-group-item">Newsletter Integration</li>
                        <li class="list-group-item">90 days Post-Installation Support</li>
                    </ul>
                    <button class="btn btn-danger">Get Started</button>
                </div>
            </div>
        </div>
    </div>
@endsection