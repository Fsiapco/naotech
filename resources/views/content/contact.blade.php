@extends('index')

@section('content')
<div class="container-fluid headerGap" style="background-color:#EDF0F5;">
    <div class="row">
        <div class="col-md-6 text-justify" style="padding:10% 2%;">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem totam, saepe doloremque ad deserunt odit repudiandae aspernatur quae quaerat. Molestiae id animi quisquam necessitatibus voluptatem illum voluptates nulla sit officia.
        </div>
        <div class="col-md-6 py-5 text-center">
            <img src="{{asset('img/call_us.png')}}" class="w-50" alt="">
        </div>

    </div>

    <div class="row">
        <div class="col-md-6 m-0 p-0 ">
            <img src="{{asset('img/inst.jpg')}}" class="w-100" alt="">
        </div>
        <div class="col-md-6" style="padding:5% 2%;">
            @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
            <h3>Send us a Message</h3>

                <form method="post" action="{{route('sendContact')}}" >
                    {{ csrf_field () }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror"/>
                        <div>{{ $errors->first('name') }}</div>
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" value="{{ old('email') }}" class="form-control @error('name') is-invalid @enderror"/>
                     <div>{{ $errors->first('email') }}</div>
                    </div>

                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" cols="30" rows="10"
                        class="form-control @error('name') is-invalid @enderror">{{ old('message') }}</textarea>
                <div>{{ $errors->first('message') }}</div>
                    </div>

                    <button type="submit" class="btn btn-primary w-100">Send</button>
                </form>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6 p-5">
            <h3>You can reach us through:</h3>
            <div class="container text-center">
                <div class="row">
                    <div class="col px-5">
                        <h5 class="text-capitalize"><i class="fas fa-phone"></i>&nbsp;7754-1392 / 76240370</h5>
                        <h5 class=""><a href=""><i class="fab fa-facebook-square">&nbsp;facbook.com/naotechinc</i></h5></a>
                        <h5><i class="fas fa-envelope"></i>&nbsp;sales@naotech.com.ph</h5>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-md-6 m-0 p-0">
            <div class="mapouter">
                <div class="gmap_canvas">
                    <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=naotech&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

