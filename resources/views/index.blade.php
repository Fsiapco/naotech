<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Anton|Lobster&display=swap" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('img/naotech-logo.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    <title>Naotech Inc.</title>
</head>
<body>
    @include('partial.navigation')
    @yield('content')
    @include('partial.footer')


    <script src="{{asset('js/app.js')}}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js'></script>
    <script src="{{ asset('js/QuotationValidation.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>



    $(document).ready(function(){
        $('').on("contextmenu",function(){
            return false;
        });

        var pathname = window.location.pathname;
        if(pathname != '/'){
            $('nav').removeClass('fixed-top');
        }
    });

    $('.clientCarousel').slick({
        arrows:true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        responsive: [
            {
                breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                    }
            },
            {
                breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
            },
            {
                breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
            }
        ]
    });
    $('.web-clientCarousel').slick({
        arrows:true,
        rtl: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        responsive: [
            {
                breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                    }
            },
            {
                breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
            },
            {
                breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
            }
        ]
    });

    $(document).ready(function(){
        if(window.matchMedia('(max-width: 500px)').matches){
            $('#SubDropdown').removeClass('dropleft');
            $('#SubDropdown').addClass('dropdown');
            $('#abtheaderDesign').removeClass('w-25');
            $('#abtheaderDesign').addClass('w-75');
        }
    });
</script>
</body>
</html>
