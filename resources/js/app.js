
require('./bootstrap');
window.Vue = require('vue');

// importing defendency
import Vue from 'vue';
import Router from 'vue-router';
import Swal from 'sweetalert2';
import { Form, HasError, AlertError } from 'vform';


// importing the component
import dashboard from './components/dashboard/DashboardComponent.vue'
import contact  from './components/contact/ContactComponent.vue'
import user from './components/user/UserViewComponent.vue'
import notFound from './components/404/NotfoundComponent.vue'
import printer from './components/printer/PrinterComponent.vue'
import qoutation from './components/qoutation/QoutationComponent.vue'
import hiring from './components/hiring/HiringComponent.vue'

// using component
Vue.use(Router);
Vue.use(require('vue-moment'));

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//alert declaration
window.Form = Form;
window.Fire = new Vue();
window.Swal = Swal;
const toast = Swal.mixin({
  toast:true,
  position: 'top-end',
  showConfirmButton: false,
  timer : 3000
});
window.toast = toast;

//passport component
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);
Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);
Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);
//pagination componentE
Vue.component(
    'pagination',
     require('laravel-vue-pagination')
     );

//routes
const routes  = [
    {
        path:'/home',
        component : dashboard
    },
    {
        path : '/contact',
        component : contact
    },
    {
        path : '/user',
        component : user
    },
    {
        path : '/printers',
        component : printer
    },
    {
        path : '/qoutation',
        component : qoutation

    },
    {
        path : '/hired',
        component : hiring

    },
    {
        path : '*',
        component : notFound
    }
]

//refresh
const router = new Router({
    mode:'history',
    routes
  })

  //main
const app = new Vue({
    el: '#app',
    router,
    data:{
        search:''
    },
    methods:{
        searchIt: _.debounce(() => {
            Fire.$emit('searching');
        },1000)
    }
});
