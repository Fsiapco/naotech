$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#btnFormSubmit').click(function(e){
        e.preventDefault();

        var CompanyName = $('#CompanyName').val();
        var Address = $('#Address').val();
        var contactPerson = $('#contactPerson').val();
        var Email = $('#Email').val();
        var contactNumber = $('#contactNumber').val();
        var Unit = $('#Unit').val();

        if($('#contactUsForm').valid()){
                $.ajax({
                        type        : 'post',
                        url         :  'printer-services',
                        data        :{
                            CompanyName :CompanyName,
                            Address:Address,
                            contactPerson:contactPerson,
                            Email:Email,
                            contactNumber:contactNumber,
                            Unit:Unit
                        },
                        // dataType    : 'json',
                        success: function(data){
                                document.getElementById('contactUsForm').reset();
                                Swal.fire({
                                        title: 'Sucess!',
                                        text: 'Message Sent Successfully',
                                        type: 'success',
                                        showConfirmButton: true,
                                        timer: 15000
                                })
                        }
                });
        }else{
                $('#contactUsForm').validate({
                        rules:{
                            CompanyName: {
                                        required:true,
                                        minlength:2,
                                },
                                Email:{
                                        required:true,
                                        email:true,
                                },
                                contactNumber:{
                                        required:true,
                                        number:true,
                                        minlength:5,
                                        maxlength:11,
                                },
                                contactPerson:{
                                        required:true,
                                        minlength:5,
                                },
                                Address:{
                                    required:true,
                                    minlength:5,
                                }
                             }
                });
        }
});

});
