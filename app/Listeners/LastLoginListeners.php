<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Events\Logined;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LastLoginListeners
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logined  $event
     * @return void
     */
    public function handle(Logined $event)
    {
       $user = Auth::user();
       $user->last_login_at = Carbon::now();
       $user->save();
    }
}
