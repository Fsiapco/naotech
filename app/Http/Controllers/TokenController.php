<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function update(Request $request){
        $token = Str::random(60);
        $request->user()->forceFill([
            'api_token' => $token,
        ])->save();

        return [ 'token' => $token];
    }
}
