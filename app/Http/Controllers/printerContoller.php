<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Printer;
use App\Hiring;
class printerContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $printerDetails = Printer::all();
        return view('content.service-Printer',compact('printerDetails'));
    }

    public function main()
    {
        $Hiring = Hiring::all();
        return view('partial.main',compact('Hiring'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

}
