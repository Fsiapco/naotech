<?php

namespace App\Http\Controllers\API;

use Auth;
use App\User;
use App\Printer;
use App\Contactme;
use App\Quotatation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {

        return User::latest()->paginate(10);
    }

    public function Dashboard(){
        $user = User::all()->count();
        $contact = Contactme::all()->count();
        $printer = Printer::all()->count();
        $quotation = Quotatation::all()->count();
        $current = Auth::user();
        return response()->json([
            'user' => $user,
            'contact' => $contact,
            'printer' => $printer,
            'quotation' => $quotation,
            'current' => $current
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id)
    {

        $user = User::find($id);

        if($user->status== 'Activated'){
            $newStatus = 'Deactivated';
        }else{
            $newStatus = 'Activated';
        }
      $user->update([
            'status' => $newStatus
        ]);

        return $user;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'type' => $request['type'],
            'api_token' => Str::random(60),
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchUser()
    {
        if ($search = \Request::get('user')) {
            $users = User::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                        ->orWhere('email','LIKE',"%$search%");
            })->paginate(10);
        }else{
            $users = User::latest()->paginate(10);
        }

        return $users;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user =  User::findOrFail($id);
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'sometimes',
        ]);

        $user->name = $request['name'];
		$user->email = $request['email'];
		$user->password = Hash::make($request['password']);
		$user->type = $request['type'];
		$user->update();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $user =  User::find($id)->delete();
    }
}
