<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Hiring;
class HiringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return Hiring::latest()->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'photo' => 'required',
            'details' => 'required',
        ]);

        if(!isset($request->photo)){
            $name = 'add_printer.png';
        }
        if($request->photo){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('hiring/').$name);
        }
        $request['photo'] = $name;


        return Hiring::create([
            'name' => $request['name'],
            'photo' => $request['photo'],
            'details' => $request['details']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchhiring()
    {
        if ($search = \Request::get('hiring')) {
            $hiring = Hiring::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%");
            })->paginate(10);
        }else{
            $hiring = Hiring::latest()->paginate(10);
        }

        return $hiring;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hiring =  Hiring::findOrFail($id);

        $this->validate($request,[
            'name' => 'required',
            'photo' => 'required',
            'details' => 'required',
        ]);

        if($request->photo == $hiring->photo){
            $name = $request->photo;
        }
        if($request->photo != $hiring->photo){
            $path = public_path('hiring/').$hiring->photo;
            if(file_exists($path)){
                unlink($path);
            }

            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('hiring/').$name);
        }
        $request['photo'] = $name;


        $hiring->update([
            'name' => $request['name'],
            'photo' => $name,
            'details' => $request['details']
        ]);
        return $hiring;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $printer =  Hiring::findOrFail($id);
        $path = public_path('hiring/').$printer->photo;
         if(file_exists($path)){
                unlink($path);
            }
        $printer->delete();
    }
}
