<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Printer;
class PrinterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return Printer::latest()->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'photo' => 'required',
            'type' => 'required',
            'ink' => 'required',
            'speed' => 'required',
            'resolution' => 'required',
            'size' => 'required',
        ]);

        if(!isset($request->photo)){
            $name = 'add_printer.png';
        }
        if($request->photo){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('printer/').$name);
        }
        $request['photo'] = $name;


        return Printer::create([
            'name' => $request['name'],
            'photo' => $request['photo'],
            'type' => $request['type'],
            'ink' =>$request['ink'],
            'speed' => $request['speed'],
            'resolution' => $request['resolution'],
            'size' =>$request['size'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Printer::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $printer =  Printer::findOrFail($id);
        $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
            'photo'=>'required',
            'ink' => 'required',
            'speed' => 'required',
            'resolution' => 'required',
            'size' => 'required',
        ]);

        if($request->photo == $printer->photo){
            $name = $request->photo;
        }
        if($request->photo != $hiring->photo){
            $path = public_path('printer/').$printer->photo;
            if(file_exists($path)){
                unlink($path);
            }
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('printer/').$name);
        }
        $request['photo'] = $name;


        $printer->update([
            'name' => $request['name'],
            'photo' => $request['photo'],
            'type' => $request['type'],
            'ink' =>$request['ink'],
            'speed' => $request['speed'],
            'resolution' => $request['resolution'],
            'size' =>$request['size'],
        ]);
        return $printer;


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $printer =  Printer::findOrFail($id);
        $path = public_path('printer/').$printer->photo;

            unlink($path);

        $printer->delete();
    }
}
