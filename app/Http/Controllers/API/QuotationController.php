<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Quotatation;
use Mail;
use App\Mail\Quotation;
use App\Mail\replyQuotation;
class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return Quotatation::latest()->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = Quotatation::find($id);
        $data->update([
            'status' => 'Read',
        ]);
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quotation = Quotatation::find($id)->delete();
    }

    public function sendreply(Request $request){

        $request->validate([
            "message" => "required",
        ]);
        $email = $request['email'];
        Mail::to($email)->send(new replyQuotation($request->all()));

        return $request->all();

    }

    public function searchquotation(){
        if ($search = \Request::get('clients')) {
            $quotation = Quotatation::where(function($query) use ($search){
                $query->where('CompanyName','LIKE',"%$search%");
            })->paginate(10);
        }else{
            $quotation = Quotatation::latest()->paginate(10);
        }

        return $quotation;
    }
}
