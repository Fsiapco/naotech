<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\contactUs;
use App\Contactme;
use App\Quotatation;
use App\Mail\Quotation;
use App\Mail\replyQuotation;
class SendController extends Controller
{
    public function sendContact(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        Mail::to('gorospe.imdb@gmail.com')->send(new contactUs($request->all()));

        Contactme::create($request->all());

        return redirect('contact-us')
                ->with('message', 'Thanks for your message. We\'ll be in touch.');
    }

    public function sendQuotation(Request $request){

        Mail::to('fsiapco@gmail.com')->send(new Quotation($request->all()));

        $data =  Quotatation::create($request->all());

    }
}
