<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contactme;
use App\Quotatation;
use App\Printer;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total = Contactme::all();
        $unread = Quotatation::where('status','=','Unread');
        return view('admin.main',compact('total','unread'));
    }



}
