<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Printer;

Auth::routes(['register' => false]);

Route::get('/', 'printerContoller@main')->name('index');


Route::get('web-development', function () {
    return view('content.service-WebDev');
})->name('service-WebDev');



Route::get('printer-services', 'printerContoller@index')->name('service-Printer');

Route::get('about-us', function () {
    return view('content.about-us');
})->name('about-us');

Route::get('contact-us', function () {
    return view('content.contact');
})->name('contact-us');




Route::get('/home', 'HomeController@index')->name('home');
Route::get('{path}','HomeController@index')->where('path','([A-z\d\/_.]+)?');


// Route::any('{path}',function(){
//     return view('content.404');
// })->where('path','([A-z\d\/_.]+)?');


Route::post('contact-us', 'SendController@sendContact')->name('sendContact');
Route::post('printer-services', 'SendController@sendQuotation')->name('sendQuotation');

