<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function(){
    Route::apiResources([
        'contact' => 'API\ContactUsController',
        'user' => 'API\UserController',
        'printer'=>  'API\PrinterController',
        'quotation'=> 'API\QuotationController',
        'hiring' => 'API\HiringController'
    ]);

});


Route::get('userStatus/{id}','API\UserController@changeStatus');
Route::get('deletecontact/{id}','API\ContactUsController@destroy');
Route::get('deleteclients/{id}','API\QuotationController@destroy');

//send message
Route::post('sendReply','API\QuotationController@sendreply');

//saearch
Route::get('finduser','API\UserController@searchUser');
Route::get('findcontact','API\ContactUsController@searchContact');
Route::get('findhiring','API\HiringController@searchhiring');
Route::get('findquotation','API\QuotationController@searchquotation');
Route::get('dashboard','API\UserController@Dashboard');

