<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotatationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotatations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('CompanyName');
            $table->string('Address');
            $table->string('Email');
            $table->string('contactPerson');
            $table->string('contactNumber');
            $table->string('Unit');
            $table->string('status')->default('Unread');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotatations');
    }
}
